alias dotfiles='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
alias zshrc='nvim ~/.zshrc'
alias nvimrc='nvim ~/.config/nvim/init.vim'
alias vim='nvim'
alias pacitup='sudo pacman -Qqe > pakglist.txt'
alias stowdir='cd ~/more-dots/dots/'

function homestead
    set DIR $PWD
    cd $HOME/Homestead
    eval vagrant $argv
    cd $DIR
end

function mkcd
    mkdir -p $argv
    cd $argv
end

function mkdotfile
    set DIR $PWD
    if ! test -d $HOME/dotfiles
        mkdir $HOME/dotfiles
        git init --bare $HOME/dotfiles
        cd $HOME/dotfiles
        git config --local status.showUntrackedFiles no
        echo "dotfiles directory created and ready"
    else
        echo "Directory is already here."
    end
    cd $DIR
end

function fish_greeting
    fortune disclaimer
end

set -gx EDITOR vim
set -gx VISUAL vim

set -g man_blink -o red
set -g man_bold -o blue
set -g man_standout -b bryellow black
set -g man_underline -uo brgreen
