function! QFHistory(goNewer)
    " Get dict of properties of the current window
    let wininfo = filter(getwininfo(), {i,v -> v.winnr == winnr()})[0]
    let isloc = wininfo.loclist
    " Build the command: one of the colder/cnewer/lolder/lnewer
    let cmd = (isloc ? 'l' : 'c') . (a:goNewer ? 'newer' : 'older')
    execute cmd
endfunction

nnoremap <buffer> <Left> :call QFHistory(0)<CR>
nnoremap <buffer> <Right> :call QFHistory(1)<CR>

function! RenameFile()
    let old_name = expand('%')
    let new_name = input('New file name: ', expand('%'), 'file')
    if new_name != '' && new_name != old_name
        exec ':saveas ' . new_name
        exec ':silent !rm ' . old_name
        exec ':bd ' . old_name
        redraw!
    endif
endfunction

nnoremap <leader>fr :call RenameFile()<CR>
