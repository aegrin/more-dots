" <expr> is needed to signify an expression needs to be evaluated first - pumvisible()
inoremap <expr> <C-j> pumvisible() ? "\<C-N>" : "j"
inoremap <expr> <C-k> pumvisible() ? "\<C-P>" : "k"
cnoremap <expr> <C-j> pumvisible() ? "\<C-N>" : "j"
cnoremap <expr> <C-k> pumvisible() ? "\<C-P>" : "k"

" If you are trying to save a write protected file use this
cmap w!! w !sudo tee %

" Keybind to switch buffer by number or name
nnoremap <silent> <leader>ls :ls<CR>:b<Space>

" Move selected lines and keep proper indents
vnoremap <silent> <C-j> :m '>+1'<CR>gv=gv
vnoremap <silent> <C-k> :m '<-2'<CR>gv=gv

" window management
nnoremap <silent> <M-j> :resize -2<CR>
nnoremap <silent> <M-k> :resize +2<CR>
nnoremap <silent> <M-h> :vertical resize -2<CR>
nnoremap <silent> <M-l> :vertical resize +2<CR>
" would like to make this dynamic to catch quickfix and windows
nnoremap <silent> <BS>w :close<CR>

" Tab management
" use gt and gT as <C-tab> does not work properly
nnoremap <silent> <BS>t :tabclose<CR>

" Buffer management
nnoremap <TAB> :bnext<CR>
nnoremap <S-TAB> :bprevious<CR>
nnoremap <silent> <BS>b :b#<CR>:bd#<CR>
" nmap <M-b> :%bd<CR><C-O>:bd#<CR>

" Better tabbing because gv will reselct the last selection
vnoremap < <gv
vnoremap > >gv

" Saving instead of writing
nnoremap <C-s> :w<CR>
inoremap <C-s> <ESC>:w<CR>
vnoremap <C-s> <ESC>:w<CR>

" Fold keybinds
" toggle current fold
nnoremap <BS><BS> za
" toggle all folds
nnoremap <S-BS> zA
" fold current block
nnoremap <BS>f zfa}
" fold current paragraph
nnoremap <BS>p zfap
" delete current fold
nnoremap <BS>d zd
" delete all recursive current folds
nnoremap <S-BS>d zD
" open current fold recursivly
nnoremap <BS>g zm
" open all folds recursivly
nnoremap <S-BS>g zM
" close current fold recursivly
nnoremap <BS>a zr
" close all folds recursivly
nnoremap <S-BS>a zR
" turn current selection into a fold
vnoremap <BS>f zf
