" get to config commands!!
command! Zshrc :tabedit ~/.zshrc
command! Fishrc :tabedit ~/.config/fish/config.fish
command! FishPrompt :tabedit ~/.config/fish/functions/fish_prompt.fish
command! Nvimrc :tabedit ~/.config/nvim/init.vim
command! NvimPlugs :tabedit ~/.config/nvim/conf/base/vim-plugs.vim
command! NvimKeys :tabedit ~/.config/nvim/conf/base/keybinds.vim
command! NvimCommands :tabedit ~/.config/nvim/conf/base/commands.vim
command! NvimSettings :tabedit ~/.config/nvim/conf/base/settings.vim
command! LightlineReload call LightlineReload()
command! Homestead :tabedit ~/Homestead/Homestead.yaml

augroup general_write
    " automatically source vim files
    au BufWritePost *.vim source %
augroup END

augroup misc
    au FileType * set formatoptions-=cro
augroup END

augroup lightline_hl | au!
    au Colorscheme * call lightline#disable() | call lightline#enable()
augroup END

function! LightlineReload()
    call lightline#init()
    call lightline#colorscheme()
    call lightline#update()
endfunction
