" Change default icons
let g:signify_sign_add = '❱'
let g:signify_sign_delete = '❰'
let g:signify_sign_delete_first_line = '🠭'
let g:signify_sign_change = '🞴'

" Remove the number and text from the gutter
let g:signify_sign_show_coun = 0
let g:signify_sign_show_text =1

" Jumpting though hunks
nmap <leader>gj <plug>(signify-next-hunk)
nmap <leader>gk <plug>(signify-prev-hunk)
nnoremap <silent> <leader>gd :SignifyDiff!<CR>
